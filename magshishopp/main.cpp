#include "Customer.h"
#include <map>
#include <windows.h>


#define SIGN_BUY	1
#define UPDATE		2
#define PRINT		3
#define EXIT		4

using namespace std;

void printMenu();									// Prints the menu.
void buy(map<string, Customer> &c);					// Sign and buy.
void printList(int length);							// Prints a given list of items.
void update(map<string, Customer> &c);				// update customer shop list.
void mostPaidCustomer(map<string, Customer> &c);	// Prints the most paid customer.

Item itemList[] = { Item("Milk","00001",5.3), Item("Cookies","00002",12.6),Item("bread","00003",8.9),Item("chocolate","00004",7.0),Item("cheese","00005",15.3),Item("rice","00006",6.2),Item("fish", "00008", 31.65),Item("chicken","00007",25.99),Item("cucumber","00009",1.21),Item("tomato","00010",2.32) };

int main()
{
	int option = 0;
	map<string, Customer> abcCustomers;

	do
	{
		printMenu();
		cin >> option;
		switch (option)
		{
		case SIGN_BUY:
			buy(abcCustomers);
			continue;
		case UPDATE:
			update(abcCustomers);
			continue;
		case PRINT:
			mostPaidCustomer(abcCustomers);
			continue;
		case EXIT:
			cout << "Bye Bye see you next time." << endl;
		default:
			cout << "Invalid option" << endl;
			continue;
		}
	} while (option != EXIT);

	return 0;
}

// Prints the menu.
void printMenu()
{
	system("cls");
	cout << "Welcome to MagshiMart!"							<< endl
		 << "1.      to sign as customer and buy items"			<< endl
		 << "2.      to uptade existing customer's items"		<< endl
		 << "3.      to print the customer who pays the most"	<< endl
		 << "4.      to exit"									<< endl;
}

// Sign and buy.
void buy(map<string, Customer>& c)
{
	string s;
	int option = 0;
	cout << "Enter your name: " << endl;
	cin  >> s;
	if (s == c[s].getName())
	{
		cout << "Cannot be 2 cusomers with the same name" << endl;
	}
	else
	{
		c.insert(make_pair(s, Customer(s)));
		do 
		{
			printList(10);
			cin >> option; 
			option <= 10 && option > 0 ? c[s].addItem(itemList[option - 1]) : printf("Invalid item or exit.\n");
		} while (option != 0);
	}
}

// Prints a given list of items.
void printList(int length)
{
	cout << "The items you can buy: (0 to exit)" << endl;
	for (int i = 0; i < length; i++)
	{
		cout << i + 1 << "\t" << itemList[i].getName() << "\t     price: " << itemList[i].getUnitPrice() << endl;
	}
}

// update customer shop list.
void update(map<string, Customer>& c)
{
	int option = 0;
	string s;
	cout << "Identify" << endl;
	cin >> s;
	if (s != c[s].getName())
	{
		cout << "Customer not found " << endl;
		Sleep(500);
	}
	else
	{
		cout << "1.      Add items"    << endl
			 << "2.      Remove items" << endl
			 << "3.      Back to menu" << endl;
		cin  >> option;
		if (option < 1 || option > 2)
		{
			cout << "Returnig to main" << endl;
		}
		else if (option == 1)
		{
			do
			{
				printList(10);
				cin >> option;
				option <= 10 && option > 0 ? c[s].addItem(itemList[option - 1]) : printf("\t\tInvalid item or exit.\n\n");
			} while (option != 0);
		}
		else
		{
			cout << "todo" << endl;
			Sleep(500);
		}
	}
}

// Prints the most paid customer.
void mostPaidCustomer(map<string, Customer>& c)
{
	cout << "todo" << endl;
	Sleep(500);
}
