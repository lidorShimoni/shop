#include "Customer.h"

Customer::Customer(string name)
{
	this->_name = name;
}

Customer::Customer()
{
}

double Customer::totalSum() const
{
	set<Item>::iterator it;
	Item temp;
	double sum = 0;
	for (it = this->_items.begin(); it != this->_items.end(); it++) 
	{
		temp = *it;
		sum += temp.totalPrice();
	}
	return sum;
}

void Customer::addItem(Item itemToAdd)
{
	set<Item>::iterator it;
	Item temp;

	for (it = this->_items.begin(); it != this->_items.end(); it++)
	{
		temp = *it;
		if (*it == itemToAdd) 
		{
			this->_items.erase(it);
			temp.setCount(temp.getCount() + 1);
			this->_items.insert(temp);
			break;
		}
	}
	if (it == this->_items.end()) 
	{
		this->_items.insert(itemToAdd);
	}
}

void Customer::removeItem(Item itemToRemove)
{
	set<Item>::iterator it;
	Item temp;

	if ((it = this->_items.find(itemToRemove)) != this->_items.end())
	{
		this->_items.erase(itemToRemove);
		temp = *it;
		temp.setCount(temp.getCount() - 1);
		if (temp.getCount() != 0)
		{
			this->_items.insert(temp);
		}
	}
}

string Customer::getName()
{
	return this->_name;
}

set<Item> Customer::getItems()
{
	return this->_items;
}

void Customer::setName(string s)
{
	this->_name = s;
}

void Customer::setItems(set<Item> items)
{
	this->_items;
}
