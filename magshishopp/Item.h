#pragma once
#include<iostream>
#include<string>
#include<algorithm>
using namespace std;

class Item
{
public:
	Item();
	Item(string, string, double);
	~Item();

	void setCount(int count);
	void setName(string name);
	void setSerialNumber(string serial);
	void setUnitPrice(double unitPrice);

	int getCount();
	double getUnitPrice();
	string getName();
	string getSerialNumber();

	double totalPrice() const; //returns _count*_unitPrice
	bool operator <(const Item& other) const; //compares the _serialNumber of those items.
	bool operator >(const Item& other) const; //compares the _serialNumber of those items.
	bool operator ==(const Item& other) const; //compares the _serialNumber of those items.

	//get and set functions

private:
	string _name;
	string _serialNumber; //consists of 5 numbers
	int _count; //default is 1, can never be less than 1!
	double _unitPrice; //always bigger than 0!

};